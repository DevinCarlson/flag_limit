<?php

/**
 * @file
 * Flag limit administration and module settings UI.
 */

/**
 * Flag settings form.
 */
function flag_limit_settings_form($form, &$form_state) {
  $form = array();

  $flags = flag_get_flags();

  foreach ($flags as $flag) {
    $form['flag_limit_' . $flag->name] = array(
      '#type' => 'checkbox',
      '#title' => t('Impose a Limit on %flag', array('%flag' => $flag->get_title())),
      '#default_value' => variable_get('flag_limit_' . $flag->name, FALSE),
    );
    $form['flag_limit_' . $flag->name . '_limits'] = array(
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ':input[name="flag_limit_' . $flag->name . '"]' => array('checked' => TRUE),
        ),
      ),
    );
    $form['flag_limit_' . $flag->name . '_limits']['flag_limit_' . $flag->name . '_value'] = array(
      '#type' => 'textfield',
      '#title' => t('!flag Limit', array('!flag' => $flag->get_title())),
      '#description' => t('Maximum number of items that can be flagged per user at one time with %flag. 0 for unlimited', array('%flag' => $flag->get_title())),
      '#default_value' => variable_get('flag_limit_' . $flag->name . '_value', 0),
    );
    $form['flag_limit_' . $flag->name . '_limits']['flag_limit_' . $flag->name . '_peritem_value'] = array(
      '#type' => 'textfield',
      '#title' => t('!flag Limit Per Item', array('!flag' => $flag->get_title())),
      '#description' => t('Maximum number of times that each item can be flagged across all users with %flag. 0 for unlimited', array('%flag' => $flag->get_title())),
      '#default_value' => variable_get('flag_limit_' . $flag->name . '_peritem_value', 0),
    );
  }

  return system_settings_form($form);
}
